mlip repository
----------------------------------------------------
Ca
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. :download:`Ca-20180820<mlips/pyml.lammps.mlip-Ca-20180820>` ::

    pair_type, des_type = gaussian gtinv
    max_l, max_p, model_type, cutoff = 7 2 1 10.0
    gtinv_order, gtinv_maxl, gtinv_sym = 4 [7, 7, 2] [0, 0, 0]
    alpha =  1e-09
    rmse (energy, train) =  0.8166962384350644  (meV/atom)
    rmse (energy, test)  =  0.8259630920549218  (meV/atom)
    rmse (force, train) =  0.011115561698155089  (eV/ang)
    rmse (force, test)  =  0.011081585672530415  (eV/ang)
    rmse (stress, train) =  0.01866897602808148  (GPa)
    rmse (stress, test)  =  0.018758843378056465  (GPa)
