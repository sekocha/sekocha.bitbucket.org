.. clupan_document documentation master file, created by
   sphinx-quickstart on Tue Feb 16 22:33:03 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Linearized MLIP
--------------------------------------

Generating DFT structures
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Coming soon.

Training linearized MLIP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. Required files: train_vasprun, test_vasprun, a file for input parameters. train_vasprun and test_vasprun should include full paths for vasprun files.

   An example input file is as follows. ::

    with_force True     # True or False

    reg_method ridge    # regression method (ridge or lasso or normal)

    cutoff 10.0         # cutoff radius
    model_type 1        # 1 (power) or 2 (power + cross terms)
    max_p 2             # max power of polynomials

    alpha_min -9        # penalty min
    alpha_max -2        # penalty max
    n_alpha 8           # number of penalty parameters

    des_type gtinv      # descriptor type (pair or afs or gtinv)

    pair_type gaussian  # pairwise function type (gaussian or sph_bessel)
    gaussian_params1 1.0 1.0 1  # sequence for a in exp(-a(r-b)^2)
    gaussian_params2 0 10.0 15  # sequence for b in exp(-a(r-b)^2)

    max_l 4             # max l for afs

    gtinv_order 4           # maximum order of group-theoretic invariants
    gtinv_maxl 7 7 2 0 0    # maximum l values of group-theoretic invariants
    gtinv_sym False False False False False 
                            # symmetric invariants or non-symmetric invariants

    weight False        # weighted least squares


#. Training from vasprun files ::

    export OMP_NUM_THREADS=1 
    $(pyml)/mlip/regression.py --infile test.in
    $(pyml)/mlip/regression.py --infile test.in -l pyml.lammps.mlip -p pyml_pot

#. Saving training and test data ::

    $(pyml)/mlip/regression.py --infile test.in -write_data pyml_train_data --noreg

#. Training from saved data ::

    $(pyml)/mlip/regression.py --infile test.in -read_data pyml_train_data

Predictions by MLIP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. Property prediction ::

    $(pyml)/mlip/prediction.py --pot pyml_pot --poscar POSCAR

#. Numerical force and stress prediction ::

    $(pyml)/mlip/prediction.py --pot pyml_pot --numerical_force --poscar POSCAR
    
#. Rotationally-invariant check ::

    $(pyml)/mlip/prediction.py --pot pyml_pot --rotate_check --poscar POSCAR


Estimating uncertainty of predicted energy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Coming soon.

Example python codes for training and prediction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Saving training and test data

   An example python script is as follows. ::

    #!/usr/bin/env python
    import numpy as np
    from pyml.linpot import regression
    from pyml.common.fileio import InputParams

    p = InputParams('test.in.pair')
    tr = regression.LinearRegression(params=p, readdata='vasprun')
    tr.save_data()

#. Training from saved data

   An example python script is as follows. ::
   
    #!/usr/bin/env python
    import numpy as np
    from pyml.common.fileio import InputParams
    from pyml.linpot import regression

    p = InputParams('test.in.pair')
    reg_method = p.get_param('reg_method', default='ridge')

    tr = regression.LinearRegression\
        (readdata='binary', binary_filename='pyml_train_data')

    if (reg_method == 'ridge'):
        alpha_min = float(p.get_param('alpha_min', default=-8.0))
        alpha_max = float(p.get_param('alpha_max', default=-3.0))
        n_alpha = float(p.get_param('n_alpha', default=20))
        best_reg, scaler, best_rmse, best_alpha = \
            tr.ridge(alpha_min=alpha_min, alpha_max=alpha_max, n_alpha=n_alpha)

    tr.save_pot(file_name='pyml_pot_pair_wforce')
    tr.save_pot_for_lammps(file_name='lammps.Al.pair.mlip')


#. Energy and forces acting on atoms. An example python script is as follows. ::

    #!/usr/bin/env python
    import numpy as np
    import joblib
    from pyml.mlip.prediction import Pot

    pot = joblib.load('pyml_pot')
    e, f, s_gpa, s_ev = pot.property(file_poscar='POSCAR')


